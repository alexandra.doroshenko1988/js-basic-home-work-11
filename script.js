
// const inputIcons = document.querySelectorAll('.icon-password')
// const inputPassword = document.querySelectorAll('input[type="password"]')
// console.log(inputIcons)
// console.log(inputPassword)

// inputIcons.forEach((icon) => {
//     icon.addEventListener('click', () => {
//         if (inputPassword[index].type === 'password') {
//             inputPassword[index].type = 'text';
//             inputIcons.classList.toggle('fa-eye-slash');

//     })
// })

// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

const form = document.querySelector('.password-form');
form.addEventListener('click', (event) => {
    if (event.target.tagName === 'I') {
        event.target.classList.toggle('fa-eye-slash');
        const input = event.target.previousElementSibling;
        if (event.target.classList.contains('fa-eye-slash')) {
            input.type = 'text';
        } else {
            input.type = 'password';
        }
    }

})


const btnConfirm = document.querySelector(".btn");

btnConfirm.addEventListener("click", (event) => {
    const password = document.getElementById("input-password").value;
    const passwordConfirm = document.getElementById("confirm-password").value;

    event.preventDefault();

    if (password === "" || passwordConfirm === "") {
        errorOutput("Поля не повинні бути пусті!");
    }
    else if (password === passwordConfirm) {
        alert("You are welcome");
    }
    else {
        errorOutput("Потрібно ввести однакові значення!");
    }
});

function errorOutput(message) {
    if (document.querySelector(".attention-text") === null) {
        const btn = document.querySelector(".btn");
        const attention = document.createElement("span");
        attention.classList.add("attention-text");
        attention.innerHTML = message;
        btn.before(attention);
        setTimeout(() => attention.remove(), 2000);
    }
}

